.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
Better Typo3 Extension Configuration
=============================================================

:Version:
   |release|

:Language:
   en

:Authors:
   Valcorn

:Email:

:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license

**TYPO3**

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .

**Community Documentation**

This documentation is community documentation for the TYPO3 extension Better Typo3 Extension Configuration

It is maintained as part of this third party extension.

If you find an error or something is missing, please:
`Report a Problem <https://github.com/TYPO3-Documentation/TYPO3CMS-Example-ExtensionManual/issues/new>`__

**Extension Manual**

This documentation is for the TYPO3 extension better_ext_conf.

**For Contributors**

You are welcome to help improve this guide.
Just click on "Edit me on GitHub" on the top right to submit your change request.

.. toctree::
   :maxdepth: 3

   Introduction/Index
   Editor/Index
   Installation/Index
   Configuration/Index
   Developer/Index
   KnownProblems/Index
   ChangeLog/Index
   Sitemap
