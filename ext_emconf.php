<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Better Typo3 Extension Configuration',
    'description' => '',
    'category' => 'be',
    'author' => 'Valcorn',
    'author_email' => '',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
